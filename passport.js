import { Strategy } from 'passport-local';

import User from './models/user';
import bcrypt from 'bcryptjs';

export default function (passport) {
  passport.use(new Strategy({
    usernameField: 'email'
  }, function(email, password, done) {
      User.findOne({ email }, function (err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        bcrypt.compare(password, user.password).then(isMatch => {
          if (isMatch) {
            return done(null, user);
          } else {
            return done(null, false, {message: 'Wrong password'});
          }
        })
      });
    }
  ));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
}
