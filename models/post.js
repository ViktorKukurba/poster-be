import mongoose from 'mongoose';

const Post = mongoose.Schema({
  title: { type: String, required: true},
  content: { type: String, required: true},
  author: { type: String, required: true},
  img_src: { type: String },
  fullImagePathe: { type: String },
  receivers: { type: Array, required: true},
  date: { type: Date, required: true}
});

Post.virtual('imgFullPath').
  get(function() {
    return this.img_src ? `http://127.0.0.1:3000/${this.img_src}` : null;
  })

export default mongoose.model('Post', Post);
