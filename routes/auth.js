import express from 'express';
import User from '../models/user';
import bcrypt from 'bcryptjs';
import passport from 'passport';

const router = express.Router();

router.post('/register', (req, res) => {
  const { email, password } = req.body;

  req.checkBody('email', 'Email is required.').notEmpty();
  req.checkBody('email', 'Email is not valid.').isEmail();

  req.checkBody('password', 'Password is required.').notEmpty();
  req.checkBody('repeatPassword', 'Repeat password should match password.').equals(password);
  const errors = req.validationErrors();

  if (errors) {
    res.json({
      success: false, errors
    })
  } else {
    bcrypt.genSalt(10, 2).then(salt => bcrypt.hash(password, salt)).then(password => {
      const newUser = new User({email, password});
      return newUser.save()
    }).catch(error => {
      res.json({ success: false, error })
    }).then(user => {
      res.json(user);
    })
  }
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (error, user, info) => {
    if (error) {
      res.json({success: false, error});
    } else {
      res.json({success: true, user, info});
    }
    console.log('test0', req.user);
  })(req, res, next);
});

router.get('/user', (req, res) => {
  console.log('test', req.user);
  res.json({
    success: !!req.user,
    user: req.user
  });
});

export default router;
