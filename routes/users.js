import express from 'express';
import User from '../models/user';

const router = express.Router();


router.get('/', (req, res) => {
  User.find().then(users => {
    res.json({success: true, users});
  });
});

export default router;
