import express from 'express';
import Post from '../models/post';

const router = express.Router();

router.get('/', (req, res) => {
  Post.find().then(posts => {
    res.json({success: true, posts});
  });
});

router.post('/post', (req, res) => {
  const { author, title, content, receivers } = req.body;
  console.log('11111', req.user);
  const filePath = saveFile(req);
  const post = new Post({author, title, content, receivers, img_src: filePath, date: new Date});
  post.save().then(post => {
    res.json({success: true, post});
  }).catch(error => {
    res.json({success: false, error});
  });
});

function saveFile(req) {
  if (req.files) {
    const filePath = `images/${req.files.file.name}`;
    req.files.file.mv(`public/${filePath}`);
    return filePath;
  }
}

router.delete('/post/:id', (req, res) => {
  Post.findByIdAndDelete(req.params.id).then(post => {
    res.json({success: true, post})
  }).catch(error => {
    res.json({success: false, error});
  })
});

router.put('/post/:id', (req, res) => {
  const { author, title, content, receivers } = req.body;
  const filePath = saveFile(req);
  Post.findByIdAndUpdate(req.params.id, { author, title, content, receivers, img_src: filePath, date: new Date }, {new: true}).then(post => {
    res.json({success: true, post})
  }).catch(error => {
    res.json({success: false, error});
  })
});

export default router;
