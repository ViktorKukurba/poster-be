import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import fileUpload from 'express-fileupload';
import expressValidator from 'express-validator';
import passport from 'passport';
import flash from 'connect-flash';
import session from 'express-session';
import cookieParser from 'cookie-parser';

import passportConf from './passport';
import posts from './routes/posts';
import auth from './routes/auth';
import users from './routes/users';

const app = express();
const sessionConf = {
  secret: 'poster-app-secret',
  name: 'cookie_name',
  proxy: true,
  resave: true,
  saveUninitialized: true
}

// DB connection.
mongoose.connect('mongodb://localhost/posts_db', { useNewUrlParser: true });
mongoose.connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

// Middleware added.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
  createParentPath: true
}));
app.use(express.static('public'));
app.use(expressValidator());
app.use(flash());
app.use(cookieParser(sessionConf.secret));
app.use(session(sessionConf));
passportConf(passport);

app.use(passport.initialize());
app.use(passport.session(sessionConf));

app.use('/posts', posts);
app.use('/auth', auth);
app.use('/users', users);

// Listening port.
app.listen(3000, () => {
  console.log('App started');
});
